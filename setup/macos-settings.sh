#!/usr/bin/env zsh

# fix default audio settings
vared -p "Set or unset? [set/unset]: " -c setvar

# some defaults commands only work globally ("sudo"), some only work as the user
# I'm too lazy to figure out which one needs what
# so just set all globally and for the user simultaneously
COMMANDS=("" "sudo")
for prefix in $COMMANDS; do
    if [[ "$setvar" == "set" ]]
    then
        # bluetooth audio settings
        $prefix defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Max (editable)" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Apple Initial Bitpool (editable)" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Apple Initial Bitpool Min (editable)" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool Max" 80
        $prefix defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool Min" 80

        $prefix defaults write bluetoothaudiod "Enable AAC codec" -bool true
        $prefix defaults write bluetoothaudiod "Enable AptX codec" -bool true

        # expose settings
        $prefix defaults write com.apple.dock mru-spaces -bool false

        # Possible values:
        #  0: no-op
        #  2: Mission Control
        #  3: Show application windows
        #  4: Desktop
        #  5: Start screen saver
        #  6: Disable screen saver
        #  7: Dashboard
        # 10: Put display to sleep
        # 11: Launchpad
        # 12: Notification Center
        # 13: Lock Screen

        $prefix defaults write com.apple.dock wvous-tl-corner -int 2
        $prefix defaults write com.apple.dock wvous-tl-modifier -int 0

        $prefix defaults write com.apple.dock wvous-tr-corner -int 12
        $prefix defaults write com.apple.dock wvous-tr-modifier -int 0

        $prefix defaults write com.apple.dock wvous-bl-corner -int 4
        $prefix defaults write com.apple.dock wvous-bl-modifier -int 0

        $prefix defaults write com.apple.dock wvous-br-corner -int 5
        $prefix defaults write com.apple.dock wvous-br-modifier -int 0

        # animation speedups
        $prefix defaults write NSGlobalDomain NSWindowResizeTime -float 0.001
        $prefix defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false
        $prefix defaults write com.apple.dock expose-animation-duration -float 0.05
        $prefix defaults write com.apple.dock minimize-to-application -bool true
        $prefix defaults write com.apple.dock enable-spring-load-actions-on-all-items -bool true
        $prefix defaults write com.apple.dock show-recents -bool false

        # save panel settings
        $prefix defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
        $prefix defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

        # Disable quarantine dialog and crash reporter
        $prefix defaults write com.apple.LaunchServices LSQuarantine -bool false
        $prefix defaults write com.apple.CrashReporter DialogType -string "none"

        # set highlight colour to light violet
        $prefix defaults write NSGlobalDomain AppleHighlightColor -string "0.78823529 0.65098039 1.0"

        # enable "quit finder"
        $prefix defaults write com.apple.finder QuitMenuItem -bool true

        # screensaver settings
        $prefix defaults write com.apple.screensaver askForPassword -int 1
        $prefix defaults write com.apple.screensaver askForPasswordDelay -int 0

        $prefix defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
        $prefix defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true

        # search current folder by default
        $prefix defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

        # safari settings
        $prefix defaults write com.apple.Safari UniversalSearchEnabled -bool false
        $prefix defaults write com.apple.Safari SuppressSearchSuggestions -bool true
        $prefix defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true
        $prefix defaults write com.apple.Safari HomePage -string "about:blank"
        $prefix defaults write com.apple.Safari AutoOpenSafeDownloads -bool false
        $prefix defaults write com.apple.Safari ShowFavoritesBar -bool false
        $prefix defaults write com.apple.Safari ShowSidebarInTopSites -bool false
        $prefix defaults write com.apple.Safari IncludeInternalDebugMenu -bool true
        $prefix defaults write com.apple.Safari IncludeDevelopMenu -bool true
        $prefix defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
        $prefix defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true
        $prefix defaults write NSGlobalDomain WebKitDeveloperExtras -bool true
        $prefix defaults write com.apple.Safari AutoFillFromAddressBook -bool false
        $prefix defaults write com.apple.Safari AutoFillPasswords -bool false
        $prefix defaults write com.apple.Safari AutoFillCreditCardData -bool false
        $prefix defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false
        $prefix defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true
        $prefix defaults write com.apple.Safari SendDoNotTrackHTTPHeader -bool true
        $prefix defaults write com.apple.Safari InstallExtensionUpdatesAutomatically -bool true

        # mail settings
        $prefix defaults write com.apple.mail DraftsViewerAttributes -dict-add "DisplayInThreadedMode" -string "yes"
        $prefix defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortedDescending" -string "no"
        $prefix defaults write com.apple.mail DraftsViewerAttributes -dict-add "SortOrder" -string "received-date"
        $prefix defaults write com.apple.mail DisableInlineAttachmentViewing -bool true

        # stop .DS_Store
        $prefix defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
        $prefix defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

        # arrange by grid on desktop
        $prefix /usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
        $prefix /usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
        $prefix /usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

        # Open and save files as UTF-8 in TextEdit
        $prefix defaults write com.apple.TextEdit RichText -int 0
        $prefix defaults write com.apple.TextEdit PlainTextEncoding -int 4
        $prefix defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4

        # disable auto-substitutions
        $prefix defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
        $prefix defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
        $prefix defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
        $prefix defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

        # needs work??? keyboard setup
        #defaults write NSGlobalDomain AppleLanguages -array "en-CA" "jp-CA"
        #defaults write NSGlobalDomain AppleLocale -string "en_CA@currency=CAD"
        #defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
        #defaults write NSGlobalDomain AppleMetricUnits -bool true

        # input switching
        #defaults write com.apple.hitoolbox AppleCapsLockPressAndHoldToggleOff -bool false
        #defaults write com.apple.HIToolbox AppleEnabledInputSources -array-add '<dict><key>InputSourceKind</key><string>Keyboard Layout</string><key>KeyboardLayout ID</key><integer>29</integer><key>KeyboardLayout Name</key><string>Canadian</string></dict>'
        #defaults write com.apple.hitoolbox AppleCurrentKeyboardLayoutInputSourceID -string "com.apple.keylayout.Canadian"
        #defaults write com.apple.HIToolbox AppleEnabledInputSources -array-delete '<dict><key>InputSourceKind</key><string>Keyboard Layout</string><key>KeyboardLayout ID</key><integer>29</integer><key>KeyboardLayout Name</key><string>Canadian</string></dict>'

        # don't show dialog for thumb drives
        $prefix defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

        # disable dictation
        $prefix defaults write com.apple.hitoolbox AppleDictationAutoEnable -bool false

        # Enable the automatic update check
        $prefix defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true
        $prefix defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1
        $prefix defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1
        $prefix defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1
        $prefix defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 1
        $prefix defaults write com.apple.commerce AutoUpdate -bool true


        sudo chflags nohidden /Volumes
        #sudo nvram StartupMute=%00
        #sudo spctl --master-disable

        # disable the "feature" of reopening every single app upon bluescreen or update
        # there has to be a better way...
        $prefix defaults write com.apple.loginwindow LoginwindowLaunchesRelaunchApps -bool false
        LOGIN_FILE=$(ls -c ~/Library/Preferences/ByHost/com.apple.loginwindow* | head -1)
        sudo /bin/bash -c "echo '' > $LOGIN_FILE"
        sudo chown root $LOGIN_FILE
        sudo chmod 000 $LOGIN_FILE

    elif [[ "$setvarar" == "unset" ]]
    then
        $prefix defaults delete com.apple.BluetoothAudioAgent "Apple Bitpool Max (editable)"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Apple Initial Bitpool (editable)"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Apple Initial Bitpool Min (editable)"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Negotiated Bitpool"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Negotiated Bitpool Max"
        $prefix defaults delete com.apple.BluetoothAudioAgent "Negotiated Bitpool Min"

        $prefix defaults delete bluetoothaudiod "Enable AAC codec"
        $prefix defaults delete bluetoothaudiod "AAC Bitrate"

        $prefix defaults delete NSGlobalDomain NSWindowResizeTime

        $prefix defaults delete com.apple.LaunchServices LSQuarantine

        $prefix defaults delete NSGlobalDomain NSNavPanelExpandedStateForSaveMode
        $prefix defaults delete NSGlobalDomain NSNavPanelExpandedStateForSaveMode2
        $prefix defaults delete com.apple.CrashReporter DialogType

        $prefix defaults delete NSGlobalDomain NSAutomaticCapitalizationEnabled
        $prefix defaults delete NSGlobalDomain NSAutomaticDashSubstitutionEnabled
        $prefix defaults delete NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled
        $prefix defaults delete NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled
        # Disable smart quotes as it’s annoying for messages that contain code
        $prefix defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "automaticQuoteSubstitutionEnabled" -bool false

        #sudo nvram StartupMute=%01
        #sudo spctl --master-enable
    fi
done
