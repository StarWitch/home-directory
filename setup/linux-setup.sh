#!/usr/bin/env bash

echo "Installing tools for Linux (assuming deb-based)"

echo "Basic..."
sudo apt-get install htop zsh zsh-syntax-highlighting zsh-autosuggestions vim screen tmux direnv fzf gnupg git openssh-server xdotool libinput-tools kitty-terminfo terminfo silversearcher-ag vifm -y
echo "Networking..."
sudo apt-get install nmap whois curl wget -y
echo "Dev..."
sudo apt-get install git python3-pip python3-dev -y
echo "Extra..."
sudo apt-get install neofetch lolcat fonts-firacode fonts-dejavu-extra fonts-dejavu fonts-noto -y

echo "Installing omz..."
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "Installing kitty..."
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
mkdir -p ~/.local/bin/
ln -s ~/.local/kitty.app/bin/kitty ~/.local/bin/
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
cp ~/.local/kitty.app/share/applications/kitty.desktop /usr/share/applications/
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty.desktop

while ! [[ "$install_extra" =~ ^([yY][eE][sS]|[yY])$ ]]; do
    read -p "Install extra packages? [Y/n]: " install_extra
    if [[ "$install_extra" =~ ^([nN][oO]|[nN])$ ]]
	then
        echo "Exiting..."
        exit 0
    fi
done

echo "meson, etc..."
sudo pip3 install meson==0.56.2

echo "More!!!..."
sudo apt install nodejs npm yarn -y
