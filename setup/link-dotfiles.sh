#!/usr/bin/env bash

export STAR_HOME="$HOME/.star-home"

# delete current links
rm -f ~/.zshrc ~/.vimrc ~/.tmux.conf ~/.tmux.conf.local ~/.config/kitty/kitty.conf 

# vim section
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
ln -sf $STAR_HOME/vimrc ~/.vimrc
ln -sf $STAR_HOME/vimhelp ~/.vimhelp
mkdir -p ~/.vim/undo/ ~/.vim/swap/ ~/.vim/backup/

# zsh section
curl -L https://iterm2.com/shell_integration/zsh -o ~/.iterm2_shell_integration.zsh
ln -sf $STAR_HOME/zshrc ~/.zshrc

# tmux section
#ln -sf $STAR_HOME/tmux/.tmux.conf ~/.tmux.conf
#ln -sf $STAR_HOME/tmux/.tmux.conf.local ~/.tmux.conf.local

# git stuff
ln -sf $STAR_HOME/.gitignore ~/.gitignore
git config --global core.excludesfile '~/.gitignore'

ln -sf $STAR_HOME/lavender.zsh-theme ~/.oh-my-zsh/custom/themes/lavender.zsh-theme

ln -sf $STAR_HOME/kitty.conf ~/.config/kitty/kitty.conf

mkdir -p ~/.config/fusuma/
ln -sf $STAR_HOME/fusuma-conf.yml ~/.config/fusuma/config.yml
