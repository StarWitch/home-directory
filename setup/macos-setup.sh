#!/usr/bin/env zsh

echo "Select what you want to install (space delimited):"
echo "brew - Install Homebrew"
echo "basic - Install basic CLI tools"
echo "daily - Install daily apps from Homebrew Cask"
echo "devel - Development tools"
echo "cloud - Cloud management tools"
echo "graphics - Image/video manipulation"
echo "audio - Audio processing"
echo "appstore - Mac App Store apps. Append '-nonessential' and '-heavy' for more apps."
echo "keyboard - To control the firmware on my keyboard"
echo "hobby - For 2D/3D design and microcontrollers"
echo "fonts - Third-party fonts"
echo "gamedev - For Taisei Project (mostly)"
echo "games - Installs Steam"
echo "obscure - Stuff that doesn't fit anywhere else or is seldom used"
vared -p "> " -c categories

# install Homebrew
if [[ "$categories" == *"brew"* ]]
then
    echo "Installing XCode CLI tools..."
    xcode-select --install
    if brew --version | grep -q 'command not found' 2>/dev/null
    then
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    fi

    # install zsh and bash
    # we want to use homebrew versions as they're more up to date
    if ! type $CUSTOM_PATH/bin/zsh &>/dev/null
    then
        brew install zsh
    fi

    # needs to be added to /etc/shells to work
    if ! grep -q "$CUSTOM_PATH/bin/zsh" /etc/shells
    then
        sudo bash -c "echo $CUSTOM_PATH/bin/zsh >> /etc/shells"
    fi

    if [[ "$SHELL" != "$CUSTOM_PATH/bin/zsh" ]]
    then
        chsh -s $CUSTOM_PATH/bin/zsh
    fi

    if ! type $CUSTOM_PATH/bin/bash &>/dev/null
    then
        brew install bash
    fi

    if ! grep -q "$CUSTOM_PATH/bin/bash" /etc/shells
    then
        sudo bash -c "echo $CUSTOM_PATH/bin/bash >> /etc/shells"
    fi

    # need this to install Mac App Store apps
    if ! type $CUSTOM_PATH/bin/mas &>/dev/null
    then
        brew install mas
    fi

    # upgrade utility
    brew tap buo/cask-upgrade

    if [ ! -d ~/.oh-my-zsh ]
    then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi
fi

# basic tools
if [[ "$categories" == *"basic"* ]]
then
    echo "Installing basic tools"
    echo "Basic CLI..."
    brew install vim neovim tmux reattach-to-user-namespace neofetch lolcat
    pip3 install neovim pelican
    echo "Shell plugins..."
    brew install zsh-completions zsh-autosuggestions zsh-syntax-highlighting bash-completion@2
    echo "Helpful tools..."
    brew install htop coreutils grep pv dialog direnv tree
    echo "Yubikey..."
    brew install yubico-piv-tool
    echo "Networking..."
    brew install whois curl wget gnupg openssh mosh nmap tcptraceroute rsync
    #echo "SSHFS..."
    #brew install sshfs macfuse --no-quarantine
    echo "Media tools..."
    brew install ffmpeg imagemagick youtube-dl
fi

# apps I use every day
if [[ "$categories" == *"daily"* ]]
then
    echo "Installing macOS daily apps"
    echo "Terminal..."
    brew install --no-quarantine kitty
    echo "Password management..."
    brew install --no-quarantine 1password-cli authy
    echo "Media..."
    brew install --no-quarantine iina vlc
    echo "Chat..."
    brew install --no-quarantine zoom
    echo "Utilities..."
    brew install --no-quarantine keka
    echo "UI..."
    brew install --no-quarantine rectangle
    echo "Browsers..."
    brew install --no-quarantine --cask microsoft-edge
fi

# development tools
if [[ "$categories" == *"devel"* ]]
then
    echo "Installing macOS dev"
    echo "Programming languages..."
    brew install go node npm ruby python rust
    echo "Project tools..."
    brew install meson cmake automake autoconf make pkg-config nasm ctags
    echo "Source/package management..."
    brew install git yarn pinentry-mac
    #brew install --no-quarantine github
    echo "Compilers..."
    brew install gcc llvm binutils
    echo "Utilities..."
    brew install fzf the_silver_searcher docutils osxutils jq
    brew install --no-quarantine vscodium powershell 
fi

if [[ "$categories" == *"graphics"* ]]
then
    echo "Installing graphics manipulation"
    brew install --cask --no-quarantine handbrake
fi

# music production
if [[ "$categories" == *"audio"* ]]
then
    echo "Installing audio apps"
    echo "DAWs..."
    brew install --cask --no-quarantine ableton-live-suite
    echo "Learning tools..."
    brew install --no-quarantine loopback
    echo "Conversion tools..."
    brew install --no-quarantine xld
fi

# installs off of the Mac App Store
if [[ "$categories" == *"appstore"* ]]
then
    declare -a APPSTORE_INSTALL
    APPSTORE_INSTALL=(
        497799835 # xcode
        409203825 # numbers
        409201541 # pages
        409183694 # keynote
        937984704 # amphetamine
        1365531024 # 1blocker
        1569813296 # 1password safari
    )

    for app in $APPSTORE_INSTALL; mas install $app
fi

# stuff I like but don't really need
if [[ "$categories" == *"appstore-nonessential"* ]]
then
    APPSTORE_INSTALL_NONESSENTIAL=(
        1289583905 # pixelmator
        1225570693 # ulysses
        909257784 # denied
        668208984 # Giphy
        1055511498 # journal
        411643860 # daisydisk
        1179623856 # pastebot
        1319778037 # istat
        1055511498 # day one
        1278508951 # trello
        639968404 # parcel
        803453959 # slack
    )

    for ne_app in $APPSTORE_INSTALL_NONESSENTIAL; mas install $ne_app
fi

# for giant apps I only need on one machine
if [[ "$categories" == *"appstore-heavy"* ]]
then
    APPSTORE_INSTALL_HEAVY=(
        408981434 # imovie
        434290957 # motion
        424389933 # final cut pro
        634148309 # logic pro x
    )

    for hvy_app in $APPSTORE_INSTALL_HEAVY; mas install $hvy_app
fi

# cloud management
if [[ "$categories" == *"cloud"* ]]
then
    echo "Installing cloud apps"
    echo "Docker..."
    brew install --cask --no-quarantine docker
    echo "Networking..."
    brew install --no-quarantine wireshark
    echo "Databases..."
    brew install redis
    #brew install --no-quarantine mysqlworkbench
    #echo "Google Cloud..."
    #brew install --no-quarantine google-cloud-sdk
    #echo "Other clouds..."
    #brew install awscli doctl terraform kubernetes-cli

fi

# keyboard
if [[ "$categories" == *"keyboard"* ]]
then
    echo "Installing keyboard drivers"
    brew tap qmk/qmk
    brew tap homebrew/cask-drivers
    brew install qmk
    brew install --no-quarantine qmk-toolbox
fi

# hobby tools
if [[ "$categories" == *"hobby"* ]]
then
    echo "Installing hobby tools"
    echo "Arduino..."
    brew install arduino-cli
    brew install --no-quarantine arduino
    echo "Design..."
    brew install --no-quarantine kicad autodesk-fusion360 prusaslicer
    echo "Art..."
    brew install --no-quarantine blender 
fi

# fonts
if [[ "$categories" == *"fonts"* ]]
then
    echo "Installing fonts"
    brew tap homebrew/cask-fonts
    brew install svn
    brew install --no-quarantine font-fira-code font-dejavu font-dejavu-sans-mono-for-powerline font-source-code-pro-for-powerline font-source-code-pro
fi

# gamedev tools
if [[ "$categories" == *"gamedev"* ]]
then
    echo "Installing gamedev tools"
    brew install sdl2 libzip libpng libsndfile opusfile opus-tools opus freetype webp
fi

# games
if [[ "$categories" == *"games"* ]]
then
    echo "Installing games"
    brew install --no-quarantine steam
fi

# obscure
if [[ "$categories" == *"obscure"* ]]
then
    echo "Installing obscure tools"
    brew install --no-quarantine xquartz vmware-fusion gswitch onyx
    #brew install --no-quarantine mactex-no-gui
fi

