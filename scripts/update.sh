#!/usr/bin/env zsh

# get sudo first so it doesn't pause during cask updates
sudo /bin/test 0
git -C ~/.star-home pull
sudo /bin/test 0
~/.star-home/scripts/update_software.sh
