#!/usr/bin/env zsh

sudo /bin/test 0
if [[ `uname` == "Darwin" ]]; then
    brew update
    brew upgrade
    # re-up sudo
    sudo /bin/test 0
    brew cu -a -y
elif [[ `uname` == "Linux" ]]; then
    # just assume deb-based
    sudo apt update
    sudo apt full-upgrade
fi

vim +PlugUpgrade +PlugInstall +PlugUpdate +qall

omz update
#vim +CocUpdate +CocUpdateSync +qall
