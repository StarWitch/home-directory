#!/usr/bin/env zsh

if (( $+commands[neofetch] && $+commands[lolcat] ))
then
    neofetch --config $STAR_HOME/neofetch.conf --ascii "$(find $STAR_HOME/scripts/cats/* | shuf -n 1 | xargs cat)" | lolcat
fi
