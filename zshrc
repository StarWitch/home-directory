# ZSH config

# the one and only
export EDITOR="vim"

# custom dotfiles
export STAR_HOME="$HOME/.star-home"

# apparently we can't use `brew` unless the PATH is already set
# sigh
AUTOSUGGEST="share/zsh-autosuggestions/zsh-autosuggestions.zsh"
HIGHLIGHTING="share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
if [[ `uname` == "Darwin" ]]; then
    if [[ `uname -p` == "arm" ]]; then
        CUSTOM_PATH="/opt/homebrew"
    else
        CUSTOM_PATH="/usr/local"
    fi
    source $CUSTOM_PATH/$AUTOSUGGEST
    source $CUSTOM_PATH/$HIGHLIGHTING
else
    CUSTOM_PATH="/usr"
    source $CUSTOM_PATH/$AUTOSUGGEST
    source $CUSTOM_PATH/$HIGHLIGHTING
fi

if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh-completions:$FPATH
    autoload -Uz compinit
    compinit
fi

# ohmyzsh exports
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="lavender"

# auto-updates omz
export DISABLE_UPDATE_PROMPT=true

plugins=(
    zsh-navigation-tools

    brew
    ag
    dotenv
    aliases
    colorize
    macos
    common-aliases
    gnu-utils
    history
    rsync
    sudo
    vi-mode

    1password
    mosh
    nmap
    screen
    tmux

    docker
    kubectl

    encode64
    fzf
    git
    gitignore
    branch

    node
    npm
    python
    pip
	pipenv
	pylint
	virtualenvwrapper
    pyenv
    rust
    rvm
    jsontools
)

export PATH="$HOME/.local/bin:$CUSTOM_PATH/sbin:$CUSTOM_PATH/bin:$PATH"

source $ZSH/oh-my-zsh.sh

# for (hidden) work secrets
source $STAR_HOME/work-exports

# for Taisei Project exports
source $STAR_HOME/taisei-exports

# for Another Project exports
source $STAR_HOME/another-exports

source $STAR_HOME/scripts/cat-prompt.sh

# homebrew settings
export HOMEBREW_CASK_OPTS="--no-quarantine"
export HOMEBREW_INSTALL_BADGE="🔮"
export HOMEBREW_NO_ANALYTICS=1

# for Unity/C#
export FrameworkPathOverride="/usr/local/Cellar/mono/6.12.0.122/lib/mono/4.7-api"

# for kitty
if [[ "$TERM" == "xterm-kitty" ]]; then
#    kitty +complete setup zsh | source /dev/stdin
	alias ssh="kitty +kitten ssh"
fi

# for makefiles
zstyle ':completion:*:*:make:*' tag-order 'targets'

eval "$(direnv hook zsh)"

export FZF_DEFAULT_COMMAND='ag -l'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  ag -l . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  ag -inr . "$1"
}

# commented out as it's not needed anymore (apparently)
# if [[ `uname` == "Darwin" ]]; then
# 	# because macOS's open file limit is comically small (default: 256)
# 	# this may also apply to other *BSDs as well
# 	ulimit -n 1024
# fi

alias update="$STAR_HOME/scripts/update.sh"
alias get="find . -type f -name"
alias gssh='/usr/bin/ssh'
alias dd-better='gdd bs=1M oflag=dsync status=progress'
export PATH="/usr/local/opt/ruby/bin:$PATH"

# for colorize plugin
ZSH_COLORIZE_STYLE="colorful"

# turn off telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1

export PATH="$PATH:~/.docker/bin"
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

export GOPATH="$HOME/Code/go"

source "$HOME/.cargo/env"
