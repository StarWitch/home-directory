set encoding=utf-8
set fileencodings=utf-8,iso-2022-jp,euc-jp,cp932,latin1
filetype off
set nocompatible

" needed to let floating popups exist
let g:polyglot_disabled = ['sensible']

" plugins go here
silent!call plug#begin('~/.vim/plugged')
" healt checking on startup
Plug 'rhysd/vim-healthcheck'
" defaults
Plug 'tpope/vim-sensible'
" file viewer improvements
Plug 'tpope/vim-vinegar'
" git blame
Plug 'tpope/vim-fugitive'
" UNIX helpers: SudoEdit, etc
Plug 'tpope/vim-eunuch'
" Commenting blocks out
Plug 'tpope/vim-commentary'
" better find/replace
Plug 'tpope/vim-abolish'
" folding
Plug 'pseewald/anyfold'
" themes
Plug 'drewtempelmeyer/palenight.vim'
Plug 'itchyny/lightline.vim'
" help menu
"Plug 'liuchengxu/vim-which-key'
" language support
Plug 'sheerun/vim-polyglot'
Plug 'Fatih/vim-go'
" autocomplete
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" editorconfig
Plug 'editorconfig/editorconfig-vim'
" various utilities
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" undotree
Plug 'mbbill/undotree'
" state persistence
Plug 'zhimsel/vim-stay'
" kitty
Plug 'fladson/vim-kitty'
call plug#end()

set tags=.ctagsdb;tags;

let g:coc_global_extensions = [
    \ 'coc-json',
    \ 'coc-solargraph',
    \ 'coc-pyright',
    \ 'coc-css',
    \ 'coc-html',
    \ 'coc-git',
    \ 'coc-sh',
    \ 'coc-spell-checker',
    \ 'coc-cmake',
    \ 'coc-yaml',
    \ 'coc-docker',
    \ 'coc-highlight',
    \ 'coc-lists',
    \ 'coc-yank',
    \ 'coc-rust-analyzer',
\ ]

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

"GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

syntax on
filetype plugin indent on

" leaders
let mapleader=" "
let maplocalleader=","

set backspace=indent,eol,start
set noshowmode
set nu
set ruler
set list
set wildmenu
set autoindent

" searching
set ignorecase smartcase

" tab settings (battle for the ages)
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set scrolloff=3
set autoread
set showcmd
set showmode
set ttyfast
set hlsearch

" state persistence
set viewoptions=cursor,folds,slash,unix

" undo history
set undofile
set undodir=~/.vim/undo//
set directory=~/.vim/swap//
set backupdir=~/.vim/backup//

" reload vimrc
noremap <silent> <Leader>fed :e ~/.vimrc<cr>
noremap <silent> <Leader>feR :so ~/.vimrc<cr>

" turn off/on line numbers
noremap <Tab>p :set nu!<CR>

" easy closing of all splits
noremap <Tab>q :qa!<CR>

if has("patch-8.1.1564")
  set signcolumn=number
else
  set signcolumn=yes
endif

" Prevent common mistake of pressing q: instead :q
ca Q q
ca Q! q!
ca W w
ca W! w!
ca Wq wq
ca Wq! wq!

" turn off/on line numbers
nnoremap <Tab>s :set signcolumn=no<CR>

" page up/down
noremap <C-h> <Home>
noremap <C-l> <End>
noremap <C-j> <PageUp>
noremap <C-k> <PageDown>

nnoremap <Tab><Left> <C-W><C-H>
nnoremap <Tab><Right> <C-W><C-L>
noremap <Tab><Down> <C-W><C-J>
nnoremap <Tab><Up> <C-W><C-K>

" arrow keys only indent now
nmap <Left> <<
nmap <Right> >>
vmap <Left> <gv
vmap <Right> >gv

" commenting shortcuts
vmap <Tab>/ gc
nmap <Tab>/ gcc

" themes
set termguicolors
colorscheme palenight
set background=dark

" lightline
let g:lightline = {
	\ 'colorscheme': 'palenight',
    \ 'component_function' : {
    \   'fileformat' : 'LightlineFileformat',
    \ },
    \ }

function! LightlineFileformat()
  return &filetype ==# 'netrw' ? '' : &fileformat
endfunction

let g:indentLine_char = '⦙'

autocmd FileType yaml setlocal et ts=2 sts=2 sw=2
autocmd FileType haml setlocal et ts=2 sts=2 sw=2
autocmd FileType vim setlocal et ts=2 sts=2 sw=2

" bring up help
"nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

" find/replace trailing whitespace
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

command! TrimWhitespace call TrimWhitespace()

noremap <Tab>t :call TrimWhitespace()<CR>

" netrw + vim-vinegar
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

" undotree
noremap <Tab>e :Vexplore<CR>
noremap <Tab>u :UndotreeToggle<CR>

" Fugitive Conflict Resolution
nnoremap <Tab>g :Gvdiff<CR>
nnoremap <Tab>gdh :diffget //2<CR>
nnoremap <Tab>gdl :diffget //3<CR>
nnoremap <Tab>gb :Gblame<CR>

let g:ackprg = 'ag --vimgrep'
